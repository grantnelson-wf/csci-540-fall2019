# CSCI 540: Advanced Database Systems

**NOTE: This is a live document and is subject to change throughout the
semester.**

Data is everywhere and often a database is a convenient way to store and process
it.  But is a relational database always the best way?  In this class we will
explore several advanced database models, computational paradigms
for processing large data sets, and searching (indexing) techniques.
Database models include spatial, key-value,
columnar, document, and graph; Computational paradigms for large data sets
include MapReduce and Streaming;  Searching techniques include approx-NN, LSH,
and inverted indices.

## Meeting Times

Mon, Wed, Fri 09:00-09:50, 332 Reid Hall

## Instructor

David L. Millman, Ph.D.

**Email**: david.millman@montana.edu

**Office hours**: Mon 15:00 - 15:50, Thurs 13:00-13:50, or by appointment

**Office**: Barnard Hall 359

**Github**: [dlm](https://github.com/dlm)

**Bitbucket**: [david_millman](https://bitbucket.org/david_millman/)


## Learning Outcomes

After successfully completing this course, students will be able to:

* Identify and Explain why a database or collections of databases is appropriate for a task
* Build a system using polyglot persistence
* Design and implement algorithms for searching and processing massive data sets


## Textbook

No required text book but optional are highly recommended

Optional and highly recommended:

* [Database System
  Concepts](https://www.amazon.com/Database-System-Concepts-Abraham-Silberschatz-ebook/dp/B07PPHYQGV)
  by Abraham Silberschatz, Henry F. Korth and S. Sudarshan
* [Seven Databases in Seven Weeks: A Guide to Modern Databases and the NoSQL
  Movement](https://www.amazon.com/Seven-Databases-Weeks-Modern-Movement/dp/1680502530)
  by Eric Redmond and Jim R. Wilson (7DB in reading below) (DO NOT USE 1st
  edition)
* [Probabilistic Data Structures and Algorithms for Big Data
  Applications](https://www.amazon.com/Probabilistic-Data-Structures-Algorithms-Applications/dp/3748190484)
  by Andrii Gakhov (PDS in reading below)

Others will be added as relevant.

### Prerequisites

* **CSCI 440-- Database Systems**: DBMS architecture; major database models;
  relational algebra fundamentals; SQL query language; index file structures,
  data modeling and management, entity relationship diagrams.

* Comfort with a Unix based operating system.

* Willingness to get your hands dirty installing and working with multiple

## Class schedule

The lecture schedule is subject to change throughout the semester, but here is
the current plan. Assignments and due dates will be updated as they're assigned
in class.

### Aug

| Date  | Description                                                                       | Assigned                           | Due                                   | Recommended Reading                        |
|-------|-----------------------------------------------------------------------------------|------------------------------------|---------------------------------------|--------------------------------------------|
| 08/26 | [Intro](./notes/2019-08-26.pdf)                                                   |                                    |                                       |                                            |
| 08/28 | [Environment setup](./env/README.md)                                              |                                    |                                       |                                            |
| 08/30 | Relational Store                                                                  | Homework 0                         |                                       | 7DB-Relational Day 1                       |
|       |                                                                                   |                                    |                                       |                                            |

### Sept

| Date  | Description                                                                       | Assigned                           | Due                                   | Recommended Reading                        |
|-------|-----------------------------------------------------------------------------------|------------------------------------|---------------------------------------|--------------------------------------------|
| 09/02 | NO CLASS (LABOR DAY)                                                              |                                    |                                       |                                            |
| 09/04 |                                                                                   |                                    |                                       | 7DB-Relational Day 2                       |
| 09/06 |                                                                                   | Homework 1                         | Homework 0                            | 7DB-Relational Day 3                       |
|       |                                                                                   |                                    |                                       |                                            |
| 09/09 | Column Store                                                                      |                                    |                                       |                                            |
| 09/11 |                                                                                   |                                    |                                       |                                            |
| 09/13 |                                                                                   | Homework 2                         | Homework 1                            |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 09/16 | Document Store                                                                    |                                    |                                       |                                            |
| 09/18 |                                                                                   |                                    |                                       |                                            |
| 09/20 |                                                                                   | Homework 3                         | Homework 2                            |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 09/23 | Graph Store                                                                       |                                    |                                       |                                            |
| 09/25 |                                                                                   |                                    |                                       |                                            |
| 09/27 |                                                                                   | Homework 4                         | Homework 3                            |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 09/30 | Key Value Store                                                                   |                                    |                                       |                                            |

### Oct

| Date  | Description                                                                       | Assigned                           | Due                                   | Recommended Reading                        |
|-------|-----------------------------------------------------------------------------------|------------------------------------|---------------------------------------|--------------------------------------------|
| 10/02 |                                                                                   |                                    |                                       |                                            |
| 10/04 |                                                                                   | Homework 5                         | Homework 4                            |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 10/07 |                                                                                   |                                    |                                       |                                            |
| 10/09 |                                                                                   |                                    |                                       |                                            |
| 10/11 |                                                                                   |                                    | Homework 5                            |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 10/14 |                                                                                   |                                    |                                       |                                            |
| 10/16 |                                                                                   |                                    |                                       |                                            |
| 10/18 |                                                                                   | Homework 6                         |                                       |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 10/21 |                                                                                   |                                    |                                       |                                            |
| 10/23 |                                                                                   |                                    |                                       |                                            |
| 10/25 |                                                                                   |                                    | Homework 6                            |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 10/28 |                                                                                   |                                    |                                       |                                            |
| 10/30 |                                                                                   |                                    |                                       |                                            |

### Nov

| Date  | Description                                                                       | Assigned                           | Due                                   | Recommended Reading                        |
|-------|-----------------------------------------------------------------------------------|------------------------------------|---------------------------------------|--------------------------------------------|
| 11/01 |                                                                                   | Homework 7                         |                                       |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 11/04 |                                                                                   |                                    |                                       |                                            |
| 11/06 |                                                                                   |                                    |                                       |                                            |
| 11/08 |                                                                                   |                                    | Homework 7                            |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 11/11 | NO CLASS (VETERANS DAY)                                                           |                                    |                                       |                                            |
| 11/13 |                                                                                   |                                    |                                       |                                            |
| 11/15 | Exam                                                                              |                                    |                                       |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 11/18 |                                                                                   |                                    |                                       |                                            |
| 11/20 |                                                                                   |                                    |                                       |                                            |
| 11/21 |                                                                                   |                                    |                                       |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 11/25 |                                                                                   |                                    |                                       |                                            |
| 11/27 | NO CLASS (THANKSGIVING BREAK)                                                     |                                    |                                       |                                            |
| 11/29 | NO CLASS (THANKSGIVING BREAK)                                                     |                                    |                                       |                                            |

### Dec

| Date  | Description                                                                       | Assigned                           | Due                                   | Recommended Reading                        |
|-------|-----------------------------------------------------------------------------------|------------------------------------|---------------------------------------|--------------------------------------------|
| 12/02 |                                                                                   |                                    |                                       |                                            |
| 12/04 |                                                                                   |                                    |                                       |                                            |
| 12/06 |                                                                                   |                                    |                                       |                                            |
|       |                                                                                   |                                    |                                       |                                            |
| 12/09 | (Finals week) 08:00-09:50                                                         |                                    |                                       |                                            |

### Potential Upcoming Topics:

* Relational (PosgreSQL)
* Column Stores (HBase)
* Document Stores (MongoDB, CouchDB)
* Graph Store (Neo4J)
* Key-Value Store (Redis)
* Probabilistic data structures
* Locality sensitive hashing

## Evaluation

Your grade for this class will be determined by:

* 10% Quizzes (lowest quiz is dropped)
* 35% Homework (lowest homework is dropped)
* 25% Exam
* 10% Group Presentation
* 20% Group Project

## Policies

### Attendance

Attendance in class with not be taken but students are responsible for all
material covered in class.  If you are not in class, you cannot receive credit
for quizzes. Attendance is strongly recommended.

### Assignments

There will be regular homework assignments (about every week or every other week
depending on the difficulty of the assignment) consisting
of written problems and coding exercises.  Homeworks will be
posted in the schedule.  If not specified, solutions should be submitted as a
PDF on Brightspace.
(The tool that I use for grading documents only works with PDFs, so any file
format other than PDF will receive a 0.)
Homework is due at 23:59 on the due date. Late homework will not be accepted.

You do NOT need to write up your solutions with LaTex, but I highly encourage
you to do so.  You can find some resources for getting started with latex (and
for making figures, and keeping all those files safe with git) in the [student
resources repo](https://github.com/compTAG/student-resources).

I encourage collaboration, see collaboration section for details.

### Discussion

Group discussions, questions, and announcements will take place on the
Brightspace message board.
is okay to send me a direct message or email if you have a question that you feel
is not appropriate to share with the class.  If, however, you send me an message
with a question for which the response would be useful to the rest of the class,
I will likely ask you to post publicly.

### Collaboration

Collaboration IS encouraged, however, all submitted individual work must be your
own and you must acknowledge your collaborators at the beginning of the
submission.

On any group project, every team member is expected to make a substantial
contribution. The distribution of the work, however, is up to the team.

A few specifics for the assignments.  You may:

* Work with anyone in the course.
* Share ideas with others in the course
* Help other teams debug their code or proofs.

You may NOT:

* Submit a proof or code that you did not write.
* Modify another's proof or code and claim it as your own.

Using resources in addition to the course materials is encouraged. But, be sure
to properly cite additional resources. Remember, it is NEVER acceptable to pass
others work off as your own.

Paraphrasing or quoting another's work without citing the source is a form of
academic misconduct. Even inadvertent or unintentional misuse or appropriation
of another's work (such as relying heavily on source material that is not
acknowledged) is considered plagiarism. If you have any questions about using
and citing sources, you are expected to ask for clarification. My rule of thumb
is if I am in doubt, I cite.

By participating in this class, you agree to abide by the [student code of
conduct](http://www.montana.edu/policy/student_conduct/).  Please review the
policy.

### Classroom Etiquette

Except for note taking and coding, please keep electronic devices off during
class, they can be distractions to other students. Disruptions to the class will
result in you being asked to leave the lecture and will negatively impact your
grade.

### Special needs information

If you have a documented disability for which you are or may be requesting an
accommodation(s), you are encouraged to contact me and Disabled
Student Services as soon as possible.
