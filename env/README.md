# Setting up your environment

In the first part of the semester, we will get some hands on experience with
multiple NoSQL systems.  To avoid some of the challenges in running on multiple
environments, I will (hopefully) be able to provide you with a set of containers
that you can run on your system.

This document will help you get setup and going.  Install docker for your
environment [see install docs](https://docs.docker.com/install/).

# Setting up the client

Note, I am planning on building one container called client that contains most
of the clients for interacting with the database (but we will see how that
goes). The client container will change throughout the semester so you may need
to rebuild it periodically.  To build the client:

    cd clients
    docker build -t adv-db/clients .

# Setting up Postgres

Building is similar to other containers:

    cd postgres
    docker build -t adv-db/postgres .

And to run the container

    docker run --net=host -p 5432:5432 adv-db/postgres

Assuming that you have built the client, we can get a psql shell by running the
following commands

    docker run --net=host -it adv/clients psql -h localhost -U postgres postgres

To make sure everything worked, in the clients shell run a few "erronius" commands
that produce an error and you should see error messages in the adv-db/postgres
container. For example when I run

    postgres=# not-a-command;

I see the following error in the container running postgres

    2019-08-28 02:07:14.921 UTC [64] STATEMENT:  not-a-command;

You should be good to go!




